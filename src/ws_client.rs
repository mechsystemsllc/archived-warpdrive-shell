use ws::{connect, Builder, Handler, Sender, Handshake, Result, Message, CloseCode, Token};

#[path = "message/create_actor_command_generated.rs"]
mod create_actor_command_generated;

use create_actor_command_generated::warpdrive::{CreateActorCommand, CreateActorCommandArgs};

#[path = "message/create_actor_command_response_generated.rs"]
mod create_actor_command_response_generated;

use create_actor_command_response_generated::warpdrive::get_root_as_create_actor_command_response;

use slog::*;

trait WsEvents {
    fn on_connected();
    fn on_disconnected();
    fn on_message_received();
    fn on_message_sent();
}

pub struct WsClient<'a> {
    log: slog::Logger,
    endpoint: Option<String>,
    handler: &'a WsEvents,
}

struct WsHandler<'a> {
    sender: Sender,
    client: &'a WsClient<'a>,
    log: slog::Logger,
}

impl WsClient {
    pub fn new(log: &slog::Logger, handler: &WsEvents) -> WsClient {
        WsClient {
            log: log.new(o!("wsclient")),
            endpoint: None,
            handler,
        }
    }

    pub fn is_connected(&self) -> bool {
        self.endpoint.is_some()
    }

    pub fn connect(&mut self, endpoint: String) {
        let worked = connect(endpoint.as_str(), |sender| WsHandler {
            sender,
            log: self.log.new(o!("wshandler")),
            client: self,
        });

        match worked {
            Ok() => {
                self.endpoint = Some(endpoint);
            }
            Err() => {
                self.endpoint = None;
            }
        }
    }

    pub fn disconnect(&mut self) {}

    fn on_open(&mut self) {
        self.handler.on_connected()
    }

    fn on_message(&mut self, msg: Message) -> Result<()> {
        if let Message::Binary(b) = msg {
            let response = get_root_as_create_actor_command_response(&b);
            trace!(self.log, "CreateActorCommandResponse received. ActorId= \"{}\"", response.actor_id().unwrap());
        } else {
            error!(self.log, "Got an invalid response");
        }

        self.out.close(CloseCode::Normal)
    }

    fn on_shutdown(&mut self) {
        self.handler.on_disconnected();
    }
}

impl Handler for WsHandler {
    fn on_shutdown(&mut self) {
        self.client.on_shutdown();
    }

    fn on_open(&mut self, _: Handshake) -> Result<()> {
        self.client.on_open();

        //Send CreateActorCommand
        let mut builder = flatbuffers::FlatBufferBuilder::new_with_capacity(1024);
        let ship = builder.create_string("BOGUS_SHIP");
        let js = builder.create_string("function f(a,b) { return a + b; }");
        let js_file = builder.create_string("add.js");

        let cmd = CreateActorCommand::create(&mut builder, &CreateActorCommandArgs {
            ship: Some(ship),
            source: Some(js),
            file_name: Some(js_file),
        });

        builder.finish(cmd, None);

        self.out.send(builder.finished_data())
    }

    // `on_message` is roughly equivalent to the Handler closure. It takes a `Message`
    // and returns a `Result<() >`.
    fn on_message(&mut self, msg: Message) -> Result<()> {
        self.client.on_message(msg);
    }

    fn on_close(&mut self, code: CloseCode, reason: &str) {
    }

    fn on_error(&mut self, err: Error) {
    }

    fn on_timeout(&mut self, event: Token) -> Result<()> {
    }
}