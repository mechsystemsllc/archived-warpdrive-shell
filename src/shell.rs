extern crate linefeed;
extern crate rand;
extern crate dirs;

use std::io;
use std::sync::Arc;

use linefeed::{Interface, Prompter, ReadResult};
use linefeed::complete::{Completer, Completion};
use linefeed::inputrc::parse_text;
use linefeed::terminal::Terminal;
use ansi_term::{Color, Style};
use self::linefeed::DefaultTerminal;
use crate::ws_client::WsClient;
use slog_scope;

static SHELL_COMMANDS: &[(&str, &str)] = &[
    ("get", "Print the value of a variable"),
    ("help", "You're looking at it"),
    ("variables", "List variables"),
    ("history", "Print history"),
    ("save-history", "Write history to file"),
    ("exit", "Exit the shell"),
    ("set", "Assign a value to a variable"),
    ("connect", "Connect to a Warpdrive node"),
    ("disconnect", "Disconnect from the Warpdrive node"),
];

trait ColorPrompt {
    fn set_prompt_with_color(&self, style: &Style, text: &str) -> io::Result<()>;
}

impl ColorPrompt for Interface<DefaultTerminal> {
    fn set_prompt_with_color(&self, style: &Style, text: &str) -> io::Result<()> {
        self.set_prompt(&format!("\x01{prefix}\x02{text}\x01{suffix}\x02",
                                 prefix = style.prefix(),
                                 text = text,
                                 suffix = style.suffix()))
    }
}

pub fn run(log: slog::Logger) -> io::Result<()> {
    let interface = Arc::new(Interface::new("Warpdrive")?);

    println!("Enter \"help\" for a list of commands.");
    println!("Press Ctrl-D or enter \"exit\" to exit.");
    println!();

    interface.set_completer(Arc::new(DemoCompleter));

    let style = Color::Green.bold();
    let text = "> ";

    interface.set_prompt_with_color(&style, text)?;

    let mut home = dirs::home_dir().unwrap();
    home.push(".warpdrive.hst");
    let history_file = home.to_str().unwrap();

    if let Err(e) = interface.load_history(&history_file) {
        if e.kind() == io::ErrorKind::NotFound {} else {
            eprintln!("Could not load history file {}: {}", history_file, e);
        }
    }

    let mut client = WsClient::new(&log);

    while let ReadResult::Input(line) = interface.read_line()? {
        if !line.trim().is_empty() {
            interface.add_history_unique(line.clone());
        }

        let (cmd, args) = split_first_word(&line);

        match cmd {
            "connect" => {
                if client.connected {
                    println!("Already connected")
                } else {
                    client.connect(args);
                }
            }
            "disconnect" => {
                if ! client.connected {
                    println!("Not connected")
                } else {
                    client.disconnect();
                }
            }
            "help" => {
                println!("Commands:");
                println!();
                for &(cmd, help) in SHELL_COMMANDS {
                    println!("  {:15} - {}", cmd, help);
                }
                println!();
            }
            "get" => {
                if let Some(var) = interface.get_variable(args) {
                    println!("{} = {}", args, var);
                } else {
                    println!("no variable named `{}`", args);
                }
            }
            "variables" => {
                for (name, var) in interface.lock_reader().variables() {
                    println!("{:30} = {}", name, var);
                }
            }
            "history" => {
                let w = interface.lock_writer_erase()?;

                for (i, entry) in w.history().enumerate() {
                    println!("{}: {}", i, entry);
                }
            }
            "save-history" => {
                if let Err(e) = interface.save_history(&history_file) {
                    eprintln!("Could not save history file {}: {}", history_file, e);
                } else {
                    println!("History saved to {}", history_file);
                }
            }
            "quit" => break,
            "exit" => break,
            "set" => {
                let d = parse_text("<input>", &line);
                interface.evaluate_directives(d);
            }
            _ => println!("Unknown command: {:?}", line)
        }
    }

    Ok(())
}

fn split_first_word(s: &str) -> (&str, &str) {
    let s = s.trim();

    match s.find(|ch: char| ch.is_whitespace()) {
        Some(pos) => (&s[..pos], s[pos..].trim_start()),
        None => (s, "")
    }
}

struct DemoCompleter;

impl<Term: Terminal> Completer<Term> for DemoCompleter {
    fn complete(&self, word: &str, prompter: &Prompter<Term>,
                start: usize, _end: usize) -> Option<Vec<Completion>> {
        let line = prompter.buffer();

        let mut words = line[..start].split_whitespace();

        match words.next() {
            // Complete command name
            None => {
                let mut compls = Vec::new();

                for &(cmd, _) in SHELL_COMMANDS {
                    if cmd.starts_with(word) {
                        compls.push(Completion::simple(cmd.to_owned()));
                    }
                }

                Some(compls)
            }
            // Complete command parameters
            Some("get") | Some("set") => {
                if words.count() == 0 {
                    let mut res = Vec::new();

                    for (name, _) in prompter.variables() {
                        if name.starts_with(word) {
                            res.push(Completion::simple(name.to_owned()));
                        }
                    }

                    Some(res)
                } else {
                    None
                }
            }
            _ => None
        }
    }
}